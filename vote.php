<!DOCTYPE HTML>
<html>
	<head>
		<title>Vote</title>
		<script src="http://localhost:8080/socket.io/socket.io.js"> </script>
		<script src="http://code.jquery.com/jquery-1.10.1.min.js"> </script>
		<script src="./js/vote.js" type="text/javascript"> </script>
		<link rel="stylesheet" type="text/css" href="./css/vote.css">
	</head>
	<body>
		<div id="container">
			<div id="nb_users"></div>		
			<?php 
				session_start();
				if (!empty($_SESSION['pseudo'])) {
					$pseudo = $_SESSION['pseudo']; // on récupère la session avec le pseudo
				}
			?>
		</div>
		<script type="text/javascript">
			var socket = io.connect('http://localhost:8080');
			var user_name = sessionStorage.getItem('user_name'); 
			if(user_name == null) // si user_name n'a pas encore été stocké dans une sessionStorage alors on le récupère par le php
			{
				user_name = '<?php echo $pseudo ?>';// on stocke dans une sessionStorage la valeur récupérée par le php
				sessionStorage.setItem('user_name',user_name); // on crée une sessionStorage 
			}
			// sinon il garde la valeur récupérée dans le storage
			socket.emit('transmit_pseudo', user_name);
		</script>
	</body>
</html>