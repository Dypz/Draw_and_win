var http = require('http');
var fs = require('fs');

var server = http.createServer(function(req,res){
	fs.readFile('../drawer.php','utf-8',function(error,content){
		res.writeHead(200,{'Content-Type':'text/php'});
		res.end(content);
	});
	fs.readFile('../vote.php','utf-8',function(error,content){
		res.writeHead(200,{'Content-Type':'text/php'});
		res.end(content);
	});
	fs.readFile('../guesser.php','utf-8',function(error,content){
		res.writeHead(200,{'Content-Type':'text/php'});
		res.end(content);
	});
});

var io = require('socket.io').listen(server);

/************ Variables globales nécessaires pour la mise en place du jeu **************************/
var nb_users = 0;	//nombre de users
var vote_result = []; // contient les résultats des votes
var tab_users = {};  //tableau associatif qui stocke l'ordre de connexion des pseudos
var connexion_order = 1; // itérateur pour identifier l'ordre de connexion des users
var id_drawer; // id du mec qui va dessiner
var drawer_pseudo = {};


/********************** Variables globales nécessaires pour le fonctionnement du jeu ********************************/
var to_draw;
var nb_tour = 1;

/***************** Variable globale nécessaire pour le fonctionnement du chat ****************************/
var users = {};

/*********************** Fonction pour recopier le contenu d'un tableau src vers un autre tableau dest ****************/
function cloneTab(src, dest){
	for (var val in src) {
		dest[val] = src[val];
	}
}

/****************************** Mise à jour en temps réel de tous les utilisateurs *************************************/
io.sockets.on('connection',function(socket){
	socket.on('transmit_pseudo', function(user_pseudo){
		socket.user_name = user_pseudo; // création de la variable de session contenant le pseudo de l'utilisateur qui vient de se connecter
		var next_user = nb_users +1 ;	//permet d'ajouter un nouveau user (jusque 4)
		if(next_user <= 4 )	//si 'on est pas déja 4
		{
			//On incrémente et on envoie le nombre d'users connectés aux clients
			//On associe un pseudo à la page de connexion
			nb_users++;
			socket.emit('send_nb_users',nb_users);
			socket.broadcast.emit('send_nb_users',nb_users);
			socket.emit('send_id',socket.user_name);
			tab_users[socket.user_name] = connexion_order;
			connexion_order++;
		}
	});


	/************************ Partie pour le fonctionnement du chat **************************************************/
	
	socket.on('user_name',function(user_name){
		users[socket.user_name] = true; // true si l'utilisateur est connecté
		socket.emit('new_connection',socket.user_name); // pour que le message apparaisse aussi dans le chat du user qui vient de se connceter
		socket.broadcast.emit('new_connection',socket.user_name,users); // on informe les aures utilisateurs du chat de la connection du nouvel utilisateur
	});

	socket.on('actualise_connected_users',function(){
		// dès qu'il y a une modification du nombre d'utilisateurs, on en informe les utilisateurs
		socket.emit('actualise',users);
		socket.broadcast.emit('actualise',users);
	});

	socket.on('new_message',function(content){ // dès qu'un message est envoyé par un utilisateur, on l'envoie également aux autres utilisateurs
		socket.broadcast.emit('send_message_users','<u><strong>'+socket.user_name + ':</strong></u> '+ content);
	});

	socket.on('disconnect_chat',function(user_name){
		users[socket.user_name] = false; // false si l'utilisateur se déconnecte
		socket.broadcast.emit('new_disconnection',socket.user_name,users);
	});
	

	/************************************* Partie pour accéder au jeu en lui-même *********************************************************/
	
	socket.on('user_vote',function(result){
		vote_result.push(result);	//On récupère le vote du client dans un tableau		
		if(vote_result.length == nb_users) // si tous les utilisateurs ont voté
		{
			//on vérifie que tout le monde a accepté
			//sinon on renvoie sur la page de vote
			var bool = true;
			for(var i=0;i<vote_result.length;i++)
			{
				if(vote_result[i] == 'no')
				{
					bool = false;
					break;
				}

			}
			vote_result = [];

			//si tout le monde a accepté
			//on génère un nombre aléatoire entre 1 et nb_users
			//pour déterminer quel user va dessiner le premier
			if(bool == true)
			{
				id_drawer =Math.floor((Math.random()*nb_users)+1);	//l'id de l'user qui va dessiner, généré aléatoirement
				for(var val in tab_users)
				{
					//pour chaque pseudo du tableau 
					//on regarde si la valeur qu'on a associée à un pseudo correspond au nombre aléatoire
					//si c'est le cas on arrête la boucle : val correspond alors
					//au pseudo de l'utilisateur qui va dessiner
					if(tab_users[val] == id_drawer)
					{
						break;
					}
				}
				cloneTab(tab_users, drawer_pseudo); // on recopie tous les pseudos des utilisateurs commençant la partie pour qu'il dessine ensuite tous une fois
				//on lance le jeu pour tous les utilisateurs
				socket.emit('start_game',val);
				socket.broadcast.emit('start_game',val);
			}
			else
			{
				//cas où un utilisateur refuse de lancer la partie
				socket.emit('no_game');
				socket.broadcast.emit('no_game');
			}
		}
	});

	//gestion de la déconnexion d'un client
	socket.on('disconnect',function(pseudo){
		//on décrémente nb_users
		//on efface le pseudo de l'utilisateur déconnecté
		nb_users--;
		var next_connection_order = connexion_order-1;
		if(next_connection_order >=1)
		{
			connexion_order--;
		}
		delete tab_users[pseudo]; // on supprime du tableau des utilisateurs le pseudo de la personne venant de se déconnecter
		if(nb_users <2)	//si on est en dessous du nombre minimal de joueurs
		{
			//on arrête la partie
			socket.emit('stop_game',nb_users);
			socket.broadcast.emit('stop_game',nb_users);
			connexion_order = 1;
		}
		socket.emit('send_nb_users',nb_users);
		socket.broadcast.emit('send_nb_users',nb_users);
	});


	/************************************************** Partie concernant le fonctionnement du jeu **********************************/
	socket.on('transmit_word',function(word){
		to_draw = word; // on récupère le mot devant être dessiné par le drawer
	});

	socket.on('send_user_answer',function(answer,pseudo){
		if(answer == to_draw) // si la réponse donnée par l'utilisateur est la bonne
		{
			 // alors on informe les utilisateurs du succès
			socket.emit('success',pseudo,answer);
			socket.broadcast.emit('success',pseudo,answer);
		}
		else
		{
			// sinon on ne prévient que l'utilisateur ayant répondu de sa mauvaise réponse
			socket.emit('fail',answer);
		}
	});

	socket.on('update_game',function(pseudo){
		if(nb_tour == nb_users)
		{
			// si chaque utilisateur a dessiné une fois alors la partie est terminée
			socket.emit('end_game'); 
			socket.broadcast.emit('end_game');
			nb_tour = 1; // on réinitialise le nombre de tours
		}
		else
		{
			// sinon il reste des joueurs devant dessiner
			delete drawer_pseudo[pseudo]; // on supprime le pseudo de ce qui ont déjà dessiné
			var drawer_found = false;
			while(!(drawer_found))
			{
				id_drawer = Math.floor((Math.random()*nb_users)+1); // on génère un nombre aléatoire entre 1 et nb_users correspondant à l'id de celui qui va dessiner
				for(var val in drawer_pseudo)
				{
					if(drawer_pseudo[val] == id_drawer) 
					{
						// si l'id correspond à celui d'une personne n'ayant pas encore déssiné alors on casse la boucle
						drawer_found = true;
						break;
					}
				}
			}
			nb_tour++; // on incrémente le nombre de tour effectué
			connexion_order = 1;
			socket.emit('swap_game',val);
			socket.broadcast.emit('swap_game',val);	
		}
	});

	/*********************** Partie pour la mise à jour du canvas de tous les utilisateurs *******************/

	socket.on('actualise_draw',function(initial_point,final_point,color,width){
		// on transmet toutes les informations nécessaires aux guessers pour la mise à jour de leur canvas
		// ce sont : le point de départ, le point final, la couleur et l'épaisseur du "trait"
		var point_with_context = {
			initial_point : initial_point,
			final_point : final_point,
			new_color : color,
			new_width : width
		}
		socket.broadcast.emit('actualise_users',point_with_context);
	});

	socket.on('clear_user',function(){
		// si le drawer clear son canvas, on clear également celui des autres utilisateurs
		socket.broadcast.emit('clear_draw');
	});
});

server.listen(8080); // on écoute le localhost
