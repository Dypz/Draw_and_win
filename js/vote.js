var socket = io.connect('http://localhost:8080');

function create_vote(nb_users)
{
	$('#nb_users').html('Nb users : '+nb_users+', you can be 4 at most. Do you want to launch the game ? Others users can not join if the game is started</br>')
	$('#nb_users').append("<form method='get' id='vote_result'><input type='radio' name='choix' value='yes' id='yes' /><input type='radio' name='choix' value='no' id='no' checked /><div class='switch'><label for='yes'>Yes</label><label for='no'>No</label><span></span></div><input type='submit' value='CONFIRM' id='get_user_vote' /></form>");
}

socket.on('send_nb_users',function(nb_users){
	if(nb_users>=2)
	{
		// il faut au moins 2 joueurs pour lancer une partie
		// si c'est le cas on leur demande s'ils veulent lancer la partie même s'ils ne sont pas 4
		create_vote(nb_users);
	}
	else
	{
		// si on a moins de 2 joueurs, on ne peut pas lancer la partie
		$('#nb_users').html('Nb users : '+nb_users+' , you need to be at least 2 players');
	}
});

socket.on('send_id',function(id){
	if(navigator.pseudo == undefined)
	{
		navigator.pseudo = id;
	}
});

$(document).ready(function(){
	$('#nb_users').on('submit', '#vote_result', function(event){
	event.preventDefault();
	var result = $('#vote_result input:checked').val(); // on récupère ce qui a été répondu au vote par l'utilisateur
	console.log(result);
	socket.emit('user_vote',result);
	$('#nb_users').html('You have voted, waiting for the others to vote ...	');
	});

	socket.on('start_game',function(pseudo){
		// on lance le jeu pour chaque utilisateur avec un rôle défini, pseudo représentant le drawer
		if(navigator.pseudo == pseudo)
		{
			setTimeout(function () {window.location.href = "../Projet_JS/drawer.php";}, 100);	
		}
		else
		{
			setTimeout(function () {window.location.href = "../Projet_JS/guesser.php";}, 100);
		}
	});
});

socket.on('no_game',function(){
	// si un joueur a refusé de lancer la partie, on ne la lance pas
	$('#nb_users').html('One player refused to start the game');
	setTimeout(function () {window.location.href = "../Projet_JS/vote.php";}, 2000);
});


/******************************************* Déconnexion d'un utilisateur *******************************************/
$(window).bind('beforeunload',function(){ // dès qu'un utilisateur ferme l'onglet, on informe le serveur de sa déconnexion
	socket.emit('disconnect',user_name);
});