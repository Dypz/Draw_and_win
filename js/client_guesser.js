var socket = io.connect('http://localhost:8080');

/******************************************* Gestion du canvas et de sa mise à jour en temps réel *************************************/

var canvas, context;

function my_draw(){
	//on veut dessiner dans la zone du canvas
	canvas = document.getElementById("my_canvas");
	context = canvas.getContext("2d");
}

//supprime le dessin
function clear_canvas(){
	context.clearRect(0, 0, 800, 800);
	//socket.emit('clear_user');
}


socket.on('actualise_users',function(point_with_context){
	context.beginPath();
	context.strokeStyle = point_with_context.new_color;
	context.lineWidth = point_with_context.new_width;
	context.moveTo(point_with_context.initial_point.x,point_with_context.initial_point.y);
	context.lineTo(point_with_context.final_point.x,point_with_context.final_point.y);
	context.stroke();
});

socket.on('send_id',function(id){
	if(navigator.pseudo == undefined)
	{
		navigator.pseudo = id;
	}
});

socket.on('clear_draw',function(){
	// lorsque le drawer a clear son canvas, on clear également celui des guessers
	clear_canvas();
});

socket.on('stop_game',function(nb_users){
	// il n'y a plus assez de joueurs pour continuer
	$('body').html('One player disconnected and not enough players to continue');
	setTimeout(function () {window.location.href = "../Projet_JS/vote.php";}, 2000);
});

function sendAnswer(value)
{
	// on transmet la réponse envoyée par l'utilisateur qui est convertie en majuscule tout comme le mot récupéré dans la BDD
	socket.emit('send_user_answer',value,navigator.pseudo);
}

socket.on('success',function(pseudo,answer){
	// évènement lorsqu'un un utilisateur a trouvé la bonne réponse
	if(navigator.pseudo == pseudo)
	{
		// dans ce cas c'est cet utilisateur qui a trouvé la bonne réponse -> on le félicite
		$('#guess_it').html('You found the answer, GG !');
	}
	else
	{
		// si ce n'est pas cet utilisateur qui a trouvé la bonne réponse, on l'informe de la personne qui l'a trouvée
		$('#guess_it').html(pseudo + ' found the answer : '+answer);
	}
});

socket.on('fail',function(answer){
	// lorsque l'utilisateur s'est trompé en l'informe en lui rappelant sa réponse
	$('#guess_it').html(answer+' is wrong, TRY AGAIN </br>');
});


socket.on('swap_game',function(pseudo){
	// on échange les rôles et pseudo représente le pseudo de l'utilisateur qui doit dessiner
	if(navigator.pseudo == pseudo)
	{
		setTimeout(function () {window.location.href = "../Projet_JS/drawer.php";}, 10);	
	}
	else
	{
		setTimeout(function () {window.location.href = "../Projet_JS/guesser.php";}, 10);
	}
});

socket.on('end_game',function(){
	$('body').html('Everybody has played, the game is now finished');
	setTimeout(function () {window.location.href = "../Projet_JS/index.php";}, 5000);
});

/******************************************* Déconnexion d'un utilisateur *******************************************/
$(window).bind('beforeunload',function(){ // dès qu'un utilisateur ferme l'onglet, on informe le serveur de sa déconnexion
	socket.emit('disconnect',user_name);
});