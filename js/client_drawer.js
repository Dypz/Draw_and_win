var socket = io.connect('http://localhost:8080');

/******************************************* Gestion du canvas et de sa mise à jour en temps réel *************************************/

var canvas, context;
var initial_point, final_point;
var draw = false;
var x, y;

function my_draw(){
	//on veut dessiner dans la zone du canvas
	canvas = document.getElementById("my_canvas");
	context = canvas.getContext("2d");
	
	//couleur et épaisseur par défault
	context.strokeStyle = "black";
    context.lineWidth = "1";

	//on ajoute des evenements en fonction de ce que l'utilisateur fera
	canvas.addEventListener("mousedown", click, false);
	canvas.addEventListener("mouseup", unclick, false);
	canvas.addEventListener("mousemove", move, false);
}

function click(){
	//si on clique, on commence à dessiner un chemin à partir des coordonnées de la souris (x, y)
	context.beginPath();
	context.moveTo(x, y);
	initial_point = {
		x : x,
		y : y
	};

	//si on clique, on commence à dessiner, donc draw passe à true
	draw = true;
}

function getCursorCoord(event){
	//récupère les coordonnées du curseur s'il est dans le canvas	
	var mouseX = 0;
	var mouseY = 0;
	
	while(event && !isNaN(event.offsetLeft) && !isNaN(event.offsetTop)) {
        mouseX += event.offsetLeft - event.scrollLeft;
        mouseY += event.offsetTop - event.scrollTop;
        event = event.offsetParent;
    }
    return { top: mouseY, left: mouseX };
}

function unclick(){
	//si on declique, on arrête de dessiner, donc draw passe à false
	if(draw){
		draw = false;
	}
}

function move(event){
	if(event.offsetX || event.offsetY) {
		x = event.pageX - getCursorCoord(document.getElementById("my_canvas")).left - window.pageXOffset;
		y = event.pageY - getCursorCoord(document.getElementById("my_canvas")).top - window.pageYOffset;
	}
	else if(event.layerX || event.layerY) {
		x = (event.clientX + document.body.scrollLeft + document.documentElement.scrollLeft) - getCursorCoord(document.getElementById("my_canvas")).left - window.pageXOffset;
		y = (event.clientY + document.body.scrollTop + document.documentElement.scrollTop) - getCursorCoord(document.getElementById("my_canvas")).top;
	}			
	// If started is true, then draw a line
	if(draw) {
		context.lineTo(x, y);
		final_point={
			x : x,
			y : y
		};
		context.stroke();	
		socket.emit('actualise_draw',initial_point,final_point,context.strokeStyle,context.lineWidth);
		initial_point = final_point;
	}
}

//supprime le dessin
function clear_canvas(){
	context.clearRect(0, 0, 800, 800);
	socket.emit('clear_user');
}

//modifie la couleur
function newColor(color){
	context.strokeStyle = color;
}

//modifie l'épaisseur du trait
function newSize(width){
	context.lineWidth = width;
}

function PenRed(){
    var canvas = document.getElementById('my_canvas');
    canvas.style.cursor= "url('../Projet_JS/images/CrayonRed.png'), pointer";
}

function PenBlue(){
    var canvas = document.getElementById('my_canvas');
    canvas.style.cursor= "url('../Projet_JS/images/CrayonBlue.png'), pointer";
}

function PenGreen(){
    var canvas = document.getElementById('my_canvas');
    canvas.style.cursor= "url('../Projet_JS/images/CrayonGreen.png'), pointer";
}

function PenYellow(){
    var canvas = document.getElementById('my_canvas');
    canvas.style.cursor= "url('../Projet_JS/images/CrayonYellow.png'), pointer";
}

function PenPurple(){
    var canvas = document.getElementById('my_canvas');
    canvas.style.cursor= "url('../Projet_JS/images/CrayonPurple.png'), pointer";
}

function PenBlack(){
    var canvas = document.getElementById('my_canvas');
    canvas.style.cursor= "url('../Projet_JS/images/CrayonBlack.png'), pointer";
}

function Gomme(){
    var canvas = document.getElementById('my_canvas');
	canvas.style.cursor= "url('../Projet_JS/images/Gomme.png'), pointer";
}

socket.on('send_id',function(id){
	if(navigator.pseudo == undefined)
	{
		navigator.pseudo = id;
	}
});

socket.on('stop_game',function(nb_users){
	// évènement lorsque le nombre de joueurs pour continuer la partie est insuffisant
	$('body').html('One player disconnected and not enough players to continue');
	setTimeout(function () {window.location.href = "../Projet_JS/vote.php";}, 2000);
});

socket.on('success',function(pseudo,answer){
	// le drawer est informé qu'un utilisateur a trouvé la bonne réponse
	$('#draw_word').html(pseudo + ' found the answer : '+answer);
	setTimeout(function(){socket.emit('update_game',navigator.pseudo);},3000); // on actualise alors le jeu
	
});

socket.on('swap_game',function(pseudo){
	// on échange les rôles et pseudo représente le pseudo de l'utilisateur qui sera le drawer
	if(navigator.pseudo == pseudo)
	{
		setTimeout(function () {window.location.href = "../Projet_JS/drawer.php";}, 10);	
	}
	else
	{
		setTimeout(function () {window.location.href = "../Projet_JS/guesser.php";}, 10);
	}
});

socket.on('end_game',function(){
	// évènement lorsque tous les utilisateurs ont dessiné une fois, la partie est alors terminée
	$('body').html('Everybody has played, the game is now finished');
	setTimeout(function () {window.location.href = "../Projet_JS/index.php";}, 5000);
});

$(window).bind('beforeunload',function(){
	// dès qu'un utilisateur ferme l'onglet, on informe le serveur de sa déconnexion
	socket.emit('disconnect',user_name);
});