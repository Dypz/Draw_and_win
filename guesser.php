<!DOCTYPE html>

<html>
	<head>
		<meta charset='utf-8' />
		<title> In-Game Guess </title>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script src="http://localhost:8080/socket.io/socket.io.js"> </script>
		<script src="http://code.jquery.com/jquery-1.10.1.min.js"> </script>
		<script src="./js/client_guesser.js" type="text/javascript"> </script>
		<link rel="stylesheet" type="text/css" href="./css/game1.css">
		<link rel="stylesheet" type="text/css" href="./css/chat.css">
	</head>
	<body onload="my_draw();">
		<div id="result"></div>
		<div id="Chat">
			<div id="title">
				<span id="box_title">Draw & Find</span>
			</div>
			<div id="chat"></div>
			<div id="user_area">
				<input type="text" id="user_message" placeholder="Type  your text here"> </input>
				<input type="submit" id="send_message" value="SEND"> </input>
			</div>
			<div id="users">
            	<div id="connected_users">
                	<span id="name_connected">Connected users</span>
                	<li id="users_list"></li>
            	</div>
        	</div>
		</div>
		<div id="result"></div>
        <div id="window"></div>
		<div id="game">
			<canvas width="595" height="278" id="my_canvas"></canvas>
			<div id="answer">
				<input type="text" id="text_answer" > </input>
				<input type="button" id="send_answer"  value="TRY ANSWER" onclick="sendAnswer((document.getElementById('text_answer').value).toUpperCase());"> </input>
			</div>
			<div id="guess">
				<span id="guess_it"> Guess what it is !</span>
			</div>
		</div>
		<div id="down"></div>
		<script type="text/javascript">
			var socket = io.connect('http://localhost:8080'); // on se connecte au localhost
			var user_name = sessionStorage.getItem('user_name');// on récupère le pseudo de l'utilisateur qui est stocké dans les sessionStorage
			socket.emit('transmit_pseudo',user_name);
			/******************************** Fonction pour afficher le nombre d'utilisateurs connectés ***********************/
			function connectedUsers(users) 
			{
				$('#users_list').html('');
				for(var user_val in users)
				{
					if(users[user_val] == true)
					{
						$('#users_list').append('<ul>'+user_val+'</ul>');
					}
				}
			}

			socket.emit('user_name',user_name);
			$('#user_message').focus();
			socket.on('actualise',function(users){ // dès que le serveur informe qu'il faut actualiser le nombre de users connectés, on le fait
				connectedUsers(users);
			});

			socket.on('new_connection',function(user_name,users){ // on informe les autres utilisateurs déjà connectés de l'arrivée d'un nouvel user
				$('#chat').prepend('<strong>'+user_name+' join the room<strong></br>');
				socket.emit('actualise_connected_users'); // on informe le serveur qu'il faut mettre à jour la liste de users connectés
			});

			$('#send_message').on('click',function(){
				if($('#user_message').val() != "") // on bloque les messages vides
				{
					var message = $('#user_message').val();
					$('#chat').prepend('<u><strong>'+user_name+':</strong></u> '+message+'</br>');// dès que le formulaire est soumis <-> message envoyé, on l'affiche sur la page du user
					socket.emit('new_message',message); // on envoie ce message au serveur pour qu'il l'envoie aux autres users
					$('#user_message').val("").focus(); // on remet la zone de texte à vide ainsi que le focus sur cette zone
				}
			});

			socket.on('send_message_users',function(content){ // affiche le message envoyé par un autre user
				$('#chat').prepend(content+'</br>');
			});

			$(window).bind('beforeunload',function(){ // dès qu'un utilisateur ferme l'onglet, on informe de sa déconnexion
				socket.emit('disconnect_chat');
			});

			socket.on('new_disconnection',function(user_name,users){
				socket.emit('actualise_connected_users');
				$('#chat').append('<strong>'+user_name+' disconnects from the room</strong></br>');
			});
		</script>
	</body>
</html>		
