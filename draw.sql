-- phpMyAdmin SQL Dump
-- version 4.1.14
-- http://www.phpmyadmin.net
--
-- Client :  localhost
-- Généré le :  Ven 13 Février 2015 à 17:14
-- Version du serveur :  5.6.17
-- Version de PHP :  5.5.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Base de données :  `draw`
--

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE IF NOT EXISTS `users` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `pseudo` varchar(30) NOT NULL,
  `password` varchar(100) NOT NULL,
  `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=10 ;

--
-- Contenu de la table `users`
--

INSERT INTO `users` (`id`, `pseudo`, `password`, `date`) VALUES
(1, 'test', 'mdp', '2015-02-11 21:39:04'),
(2, 'new', 'bonjour', '2015-02-11 21:43:10'),
(3, 'ajout', 'mdp', '2015-02-11 23:05:16'),
(7, 'pp', 'pp', '2015-02-11 23:20:04'),
(8, 'xion', 'mdp', '2015-02-13 13:30:53');

-- --------------------------------------------------------

--
-- Structure de la table `words`
--

CREATE TABLE IF NOT EXISTS `words` (
  `id` int(255) unsigned NOT NULL AUTO_INCREMENT,
  `word` varchar(50) CHARACTER SET utf8mb4 NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB  DEFAULT CHARSET=utf8 AUTO_INCREMENT=59 ;

--
-- Contenu de la table `words`
--

INSERT INTO `words` (`id`, `word`) VALUES
(1, 'Mouton'),
(2, 'Canard'),
(3, 'Chameau'),
(4, 'Etoile de mer'),
(5, 'Harry Potter'),
(6, 'Mickey'),
(7, 'Dark Vador'),
(8, 'Telephone'),
(9, 'Pyramide'),
(10, 'Escargot'),
(11, 'Clavier'),
(12, 'Electricite'),
(13, 'Horloge'),
(14, 'Voiture'),
(15, 'Fleur'),
(16, 'Chevre'),
(17, 'Capitaine Spoke'),
(18, 'Bob l''eponge'),
(19, 'Zombie'),
(20, 'Carbone'),
(21, 'Chat'),
(22, 'Diable'),
(23, 'Ordinateur'),
(24, 'Chocolat'),
(25, 'Echec'),
(26, 'Igloo'),
(27, 'Lunettes'),
(28, 'Einstein'),
(29, 'Epee'),
(30, 'Bouclier'),
(31, 'Arbalette'),
(32, 'Tuyau d''arrosage '),
(33, 'Preservatif'),
(34, 'Oreiller'),
(35, 'Lit'),
(36, 'Soleil'),
(37, 'Satellite'),
(38, 'Tour Effeil'),
(39, 'Big Bang'),
(40, 'Terre'),
(41, 'Squellette'),
(42, 'Singe'),
(43, 'Sphere'),
(44, 'Pomme'),
(45, 'Paradis'),
(46, 'Toilettes'),
(47, 'Papier'),
(48, 'Bonhomme de neige'),
(49, 'Professeur'),
(50, 'Cheveux'),
(51, 'Licorne'),
(52, 'Marteau'),
(53, 'Verre'),
(54, 'Couleur'),
(55, 'Colere'),
(56, 'Joie'),
(57, 'Peur'),
(58, 'Tristesse');

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
