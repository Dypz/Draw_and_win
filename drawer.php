<!DOCTYPE html>

<html>
	<head>
		<meta charset='utf-8' />
		<title> In-Game </title>
		<script type="text/javascript" src="js/jquery.js"></script>
		<script src="http://localhost:8080/socket.io/socket.io.js"> </script>
		<script src="http://code.jquery.com/jquery-1.10.1.min.js"> </script>
		<script src="./js/client_drawer.js" type="text/javascript"> </script>
		<link rel="stylesheet" type="text/css" href="./css/game1.css">
		<link rel="stylesheet" type="text/css" href="./css/chat.css">
	</head>

	<body onload="my_draw();">
		<div id="Chat">
			<div id="title">
				<span id="box_title">Draw & Find</span>
			</div>
			<div id="chat"></div>
			<div id="user_area">
				<input type="text" id="user_message" placeholder="Type your text here"> </input>
				<input type="submit" id="send_message" value="SEND"> </input>
			</div>
			<div id="users">
            	<div id="connected_users">
            		<div id="result"></div>
                	<span id="name_connected">Connected users</span>
                	<li id="users_list"></li>
            	</div>
        	</div>
		</div>
        <div id="window"></div>
		<div id="game">
			<canvas width="598" height="278" id="my_canvas"></canvas>
			<div id="draw_container">
				<div id="draw_word"></div>
			</div>
			<div id="palette">
				<input type="submit" onclick="newColor('blue'); PenBlue();" class="blue" value=""> </input>
				<input type="submit" onclick="newColor('red'); PenRed();" class="red" value=""> </input>
				<input type="submit" onclick="newColor('green'); PenGreen();" class="green" value=""> </input>
				<input type="submit" onclick="newColor('yellow'); PenYellow();" class="yellow" value=""> </input>
				<input type="submit" onclick="newColor('black'); PenBlack();" class="black" value=""> </input>
				<input type="submit" onclick="newColor('purple'); PenPurple();" class="purple" value=""> </input>
				<input type="submit" onclick="newColor('white'); Gomme();" class="white" value=""> </input>

				<div id="palette2">
					<input type="submit" onclick="newSize(1);" id="width1" value="1"> </input>
					<input type="submit" onclick="newSize(2);" id="width2" value="2"> </input>
					<input type="submit" onclick="newSize(5);" id="width5" value="5"> </input>
					<input type="submit" onclick="newSize(10);" id="width10" value="10"> </input>

					<div id="clear">
						<input type="submit" onclick="clear_canvas();" class="clear" value="CLEAR"> </input>
					</div>
				</div>
		    </div>	
		</div>
		<div id="down"></div>
		<?php
			error_reporting(0);
			require 'connect.php';
			
			$result = $db->query("SELECT * FROM words");
			$count = $result->num_rows;
			
			$alea = rand(1, $count);
			$req = $db->query("SELECT word FROM words WHERE id=\"".$alea."\"");
			$row = $req->fetch_array();
			$toFind = $row['word'];
			$toFind = strtoupper($toFind);
		?>
		<script type="text/javascript">
			var socket = io.connect('http://localhost:8080');
			var to_draw = '<?php echo $toFind ?>'; // on récupère le mot récupéré aléatoirement par le php dans la BDD
			var user_name = sessionStorage.getItem('user_name'); // on récupère le pseudo de l'utilisateur qui est stocké dans les sessionStorage
			socket.emit('transmit_pseudo',user_name); 
			socket.emit('transmit_word',to_draw);
			$('#draw_word').append('Word to draw : '+ to_draw);

			/***************************** Fonction pour afficher les utilisateurs connectés ********************************/
			function connectedUsers(users) 
			{
				$('#users_list').html('');
				for(var user_val in users)
				{
					if(users[user_val] == true)
					{
						$('#users_list').append('<ul>'+user_val+'</ul>');
					}
				}
			}

			socket.emit('user_name',user_name);
			$('#user_message').focus();

			socket.on('actualise',function(users){ // dès que le serveur informe qu'il faut actualiser le nombre de users connectés, on le fait
				connectedUsers(users);
			});

			socket.on('new_connection',function(user_name,users){ // on informe les autres utilisateurs déjà connectés de l'arrivée d'un nouvel user
				$('#chat').prepend('<strong>'+user_name+' join the room<strong></br>');
				socket.emit('actualise_connected_users'); // on informe le serveur qu'il faut mettre à jour la liste de users connectés
			});

			$('#send_message').on('click',function(){
				if($('#user_message').val() != "") // on bloque les messages vides
				{
					var message = $('#user_message').val();
					$('#chat').prepend('<u><strong>'+user_name+':</strong></u> '+message+'</br>');// dès que le formulaire est soumis <-> message envoyé, on l'affiche sur la page du user
					socket.emit('new_message',message); // on envoie ce message au serveur pour qu'il l'envoie aux autres users
					$('#user_message').val("").focus(); // on remet la zone de texte à vide ainsi que le focus sur cette zone
				}
			});

			socket.on('send_message_users',function(content){ // affiche le message envoyé par un autre user
				$('#chat').prepend(content+'</br>');
			});

			$(window).bind('beforeunload',function(){ // dès qu'un utilisateur ferme l'onglet, on informe de sa déconnexion
				socket.emit('disconnect_chat');
			});

			socket.on('new_disconnection',function(user_name,users){
				socket.emit('actualise_connected_users');
				$('#chat').prepend('<strong>'+user_name+' disconnects from the room</strong></br>');
			});
		</script>
	</body>
</html>		

