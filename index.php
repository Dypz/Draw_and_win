<!DOCTYPE HTML>
<html>
	<head>
		<link href="css/login.css" rel='stylesheet' type='text/css' />
		<meta https-equiv="Content-type" charset="UTF-8" name="viewport" content="width=device-width, initial-scale=1" />
		<script type="application/x-javascript"> addEventListener("load", function() { setTimeout(hideURLbar, 0); }, false); function hideURLbar(){ window.scrollTo(0,1); } </script>
		<title>Index</title>
		<link href='http://fonts.googleapis.com/css?family=Open+Sans:600italic,400,300,600,700' rel='stylesheet' type='text/css'>
		<script type="text/javascript">
			sessionStorage.clear();
		</script>
	</head>
	<body>
		<?php		//on détruit toutes les sessions en cours si on veut se connecter
			session_start ();
			session_unset ();
			session_destroy ();
		?>		
		<div class="login-form">
			<div class="head">
				<img src="images/logo.jpg" alt=""/>			
			</div>
			<form action="index.php?action=login" method="POST">
				<li>
					<input type="text" class="text" value="USERNAME" name="pseudo" id="pseudo" placeholder="pseudo" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'USERNAME';}" ><a href="#" class=" icon user"></a> </input> </br>
				</li>
				<li>
					<input type="password" name="password" id="password" placeholder="password" value="Password" onfocus="this.value = '';" onblur="if (this.value == '') {this.value = 'Password';}"><a href="#" class=" icon lock"></a> </input> </br>
				</li>
				<div class="p-container">
					<input type="submit" onclick="myFunction()" value="SIGN IN"> </input>
				</div>
				<input type=hidden name=afficher value=ok> </input> </br>
			</form>
			<a href="inscription.php"><span class="lien">S'inscrire</span> </a>
		</div>
		<?php
			if(isset($_GET['action']) && $_GET['action'] == 'login'){		//s'il essaye de se connecter
				error_reporting(0);			//Fixe le niveau de rapport d'erreurs PHP
				require 'connect.php';		//Comme include mais previent en cas d'erreur
				
				$result = $db->query("SELECT pseudo, password FROM users WHERE pseudo = \"".$_POST['pseudo']."\"");		//on envoie la requête			
				$row = $result->fetch_array();		//on récupère l'information sous forme de tableau
						
				if($row['pseudo'] == ''){		//si le pseudo n'existe pas
					echo 'Mauvais Login';
				}
				else{
					if($row['password'] == $_POST['password']){		//si le mdp est correct
						session_start();							//on démarre une session
						$_SESSION['pseudo'] = $_POST['pseudo'];				//on met pseudo et mdp dans une variable session 	
						$_SESSION['password'] = $_POST['password'];
						header('location: vote.php');			//on change de page
					}
					else{			//si le mdp est incorrect
						echo 'Mauvais Login';
					}
				}
			}			
		?>
	</body>
</html>