function fill(input, error){
	if(error){
		input.style.backgroundColor = "red";		//colore le champ en rouge si input incorrect
	}
	else{
		input.style.backgroundColor = "#7CFC00";		//colore le champ en vert si input correct
	}
}
		
function verifPseudo(pseudo){		//vérifie la validité du pseudo
	if(pseudo.value.length < 2 || pseudo.value.length > 25){ 		//si la longueur du pseudo est < 2 ou > 25
		fill(pseudo, true); 	//incorrect
		return false;
	} 
	else{		//correct
		fill(pseudo, false);
		return true;
	}
}

function verifPass(password){		//vérifie la validité du password
	if(password.value.length < 3){ 		//si la longueur du mdp est < 3
		fill(password, true); 		//incorrect
		return false;
	} 
	else{		//correct
		fill(password, false);
		return true;
	}
}
		
function verifForm(f){		//vérifie si on peut envoyer nos données
	var pseudoOk = verifPseudo(f.pseudo);
	var passOk = verifPass(f.password);
		   
	if(pseudoOk && passOk){
		return true;
	}
	else{
		alert("Veuillez remplir correctement tous les champs");
	    return false;
	}
}